package no.uib.inf101.sample;

import java.awt.Component;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class Controller implements KeyListener {
  private final Model model;
  private final Component view;

  public Controller(Model model, Component view) {
    this.model = model;
    this.view = view;

    // Capture keyboard input when the view is in focus and send it here
    this.view.setFocusable(true);
    this.view.addKeyListener(this);
  }

  @Override
  public void keyPressed(KeyEvent e) {
    this.model.increment();
    this.view.repaint();
  }

  @Override
  public void keyTyped(KeyEvent e) {
    // ignore
  }

  @Override
  public void keyReleased(KeyEvent e) {
    // ignore
  }
}

