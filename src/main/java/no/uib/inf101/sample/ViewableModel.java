package no.uib.inf101.sample;

public interface ViewableModel {
    /** Get the current count. */
    int getCount();
  }