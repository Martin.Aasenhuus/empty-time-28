package no.uib.inf101.sample;



import javax.swing.JFrame;

public class Main {
  public static void main(String[] args) {
    Model model = new Model();
    View view = new View(model);
    new Controller(model, view);

    JFrame frame = new JFrame();
    frame.setTitle("INF101");
    frame.setContentPane(view);
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.pack();
    frame.setVisible(true);
  }
}



