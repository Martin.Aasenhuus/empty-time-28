package no.uib.inf101.sample;

public class Model implements ViewableModel {
    private int count = 0;
  
    @Override
    public int getCount() {
      return this.count;
    }
    
    /** Increment the counter. */
    public void increment() {
      this.count++;
    }
  }
  
  
  
  
  
